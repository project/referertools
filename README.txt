Referertools -- Provides a variety of tools, based on a user's referer

Note: Referer is spelled wrong. Blame whoever put the misspelling in PHP, not
me.

Features:
    * Individual features for each referer
    * Multiple referers for each domain, depending on the complete URL
    * Custom pop up pages (which can be themed uniquely for each referer)
    * Per-referer themeing, to give special themes to special visitors
    
Installation and usage:
    * Go to admin/build/modules
    * Enabled the referertools.module
    * Go to admin/settings/referers
    * Create a new referer
    * Set the options appropriately
    * Click submit and you're done

Referer matching:
Referertools will search the database to find a matching referer based on the 
following patterns:

1. Exact match: If you wish to match a referer URI exactly, prepend the URI with
"http://" or "https://"
    Example: http://www.example.com/foobar
    Matches: http://www.example.com/foobar
    Does not match: http://www.example.com/foobar.html, http://www.example.com/

2. URI hierarchy matching: If you wish to match pages in a directory (or a
complex URI string from another drupal site), remove the protocol from the URL.
    Example: www.example.com/ 
    Matches: www.example.com/foobar, www.example.com/foobar.html,
             www.example.com/
    Does not match: example.com, www2.example.com
    
    Example: www.example.com/foobar
    Matches: www.example.com/foobar, www.example.com/foobar.html,
    Does not match: example.com, www2.example.com, www.example.com
    
3. Domain hierarchy matching: If the first two searches do not return a referer,
the module will use the domain for look ups. To use this matching system, only
include the domain (this is probably what you want for most applications).
    Example: example.com
    Matches: www.example.com/foobar, www.example.com/foobar.html,
             www.example.com/, example.com, www2.example.com
    Does not match: any other domain
    
    Example: .com
    Matches: www.cnn.com, www.ebay.com
    Does not match: www.redcross.org
    
    Example: deep.subdomain.example.com
    Matches: deep.subdomain.example.com/foobar, www.deep.subdomain.example.com
    Does not match: example.com, www2.example.com

To Do:
    * Update .install file for pgsql
    
Caveats:
    * Untested on pgsql
    * You cannot create referers from your $base_url. Sorry. Give me a good use
      case and I'll consider it.
    
Author:
    Mark Fredrickson <mark.m.fredrickson@gmail.com>

